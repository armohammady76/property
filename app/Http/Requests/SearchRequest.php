<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ()
    {
        return [
            "query"             => ["string", "nullable"],
            "type"              => ["integer", "max:3", "min:1", "nullable"],
            "sale"              => ["integer", "max:2", "min:1", "nullable"],
            "startArea"         => ["integer", "nullable"],
            "endArea"           => ["integer", "gt:startArea", "nullable"],
            "startRooms"        => ["integer", "nullable"],
            "endRooms"          => ["integer", "gt:startRooms", "nullable"],
            "startPrice"        => ["integer", "nullable"],
            "endPrice"          => ["integer", "gt:startPrice", "nullable"],
            "startMonthlyPrice" => ["integer", "nullable"],
            "endMonthlyPrice"   => ["integer", "gt:startMonthlyPrice", "nullable"],
        ];
    }
}
