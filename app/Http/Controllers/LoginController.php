<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Vyuldashev\LaravelOpenApi\Annotations as OpenApi;
/**
 * @OpenApi\PathItem()
 */
class LoginController extends Controller
{

    /**
     * List users.
     *
     * @OpenApi\Operation()
     * @OpenApi\Parameters(factory="LoginParameters")
     * @OpenApi\Response(factory="LoginResponse")
     */
    public function Login (LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {

            $user=Auth::user();

            $user["token"]=$user->createToken('LoginToken')->accessToken;

            return $user;

        }
    }
}
