<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SearchRequest;
use App\Property;
use Debugbar;
class SearchPropertyController extends Controller
{

    public function Search (SearchRequest $request)
    {

        $property = new Property;

        $filterConditions = self ::filters($request);
        if ($request->filled("query"))
        {
            $queryConditions = self ::query($request -> input("query"));
        }
        else
        {
            $queryConditions=[];
        }


        return $property -> where($filterConditions) -> where(function ($query) use ($queryConditions) {
            foreach ($queryConditions as $condition)
            {
                $query -> orWhere([$condition]);
            }
        }) -> get();



    }

    public static function filters (SearchRequest $request)
    {
        $filters = $request -> except("query");
        $filterConditions = [];
        foreach ($filters as $key => $filter)
        {
            switch ($key)
            {
                case "type":
                    $filterConditions[] = [
                        "type",
                        "=",
                        $filter,
                    ];
                    break;
                case "sale":
                    $filterConditions[] = [
                        "sale",
                        "=",
                        $filter,
                    ];
                    break;
                case "startArea":
                    $filterConditions[] = [
                        "area",
                        ">=",
                        $filter,
                    ];
                    break;
                case "endArea":
                    $filterConditions[] = [
                        "area",
                        "<=",
                        $filter,
                    ];
                    break;
                case "startRooms":
                    $filterConditions[] = [
                        "rooms",
                        ">=",
                        $filter,
                    ];
                    break;
                case "endRooms":
                    $filterConditions[] = [
                        "rooms",
                        "<=",
                        $filter,
                    ];
                    break;
                case "startPrice":
                    $filterConditions[] = [
                        "price",
                        ">=",
                        $filter,
                    ];
                    break;
                case "endPrice":
                    $filterConditions[] = [
                        "price",
                        "<=",
                        $filter,
                    ];
                    break;
                case "startMonthlyPrice":
                    if (!$request -> filled("sale") or $request -> input("sale") == 2)
                    {
                        $filterConditions[] = [
                            "monthly_price",
                            ">=",
                            $filter,
                        ];
                    }
                    break;
                case "endMonthlyPrice":
                    if (!$request -> filled("sale") or $request -> input("sale") == 2)
                    {
                        $filterConditions[] = [
                            "monthly_price",
                            "<=",
                            $filter,
                        ];
                    }
                    break;
            }
        }

        return $filterConditions;
    }

    public static function query ($query)
    {
        $queryConditions = [];

        foreach (Property::queryAble as $item)
        {
            $queryConditions[]=[
                $item,
                "like",
                '%'.$query.'%'
            ];
        }

        return $queryConditions;
    }
}
