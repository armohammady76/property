<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{

    const queryAble=["name","address","city","zone"];
}
