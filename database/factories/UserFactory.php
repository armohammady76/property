<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});
$factory->define(\App\Property::class, function (Faker $faker) {
    $sale=random_int(1,2);
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'type' => random_int(1,3),
        'sale' => $sale,
        'price'=>random_int(1000,1000000),
        'area'=>random_int(50,1000),
        'monthly_price'=>$sale==2?random_int(1000,1000000):0,
        'city' => $faker->city,
        'zone'=>$faker->state,
        'rooms'=>random_int(1,5),
    ];
});
